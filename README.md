# The groovy webserver

## Getting started:

Simple webserver created using groogy and gpars actors. Maven based building. Tested under unix (Ubuntu 14.04, orcacle java 1.8.0_74, groovy 2.4.3). 

## planed exercices / features:
- .htaccess like security management to disable directory traversal
- cookies
- keep-alive support
- (fast)CGI with php and executables
- basic http auth
- ssl
- external routing configuration to other methods / resources
- chunked transfer encoding for message termination: https://tools.ietf.org/html/rfc2616#section-4.4 

## sources:

http general:
https://tools.ietf.org/html/rfc2616

http keep alive:
http://tools.ietf.org/id/draft-thomson-hybi-http-timeout-01.html
http://tools.ietf.org/html/rfc7230#section-6.3
