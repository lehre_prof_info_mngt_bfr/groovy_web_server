package de.uni.leipzig.constant;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.google.common.net.MediaType;

public class TestMediaType {

	@Test
	public void testImportantMediaTypes() throws Exception {
		assertEquals("text/html; charset=utf-8", MediaType.HTML_UTF_8.toString());
		assertEquals("text/html", MediaType.HTML_UTF_8.withoutParameters().toString());
		assertEquals("text/plain", MediaType.PLAIN_TEXT_UTF_8.withoutParameters().toString());
		assertEquals("application/json", MediaType.JSON_UTF_8.withoutParameters().toString());
		assertEquals(MediaType.parse("application/javascript"), MediaType.JAVASCRIPT_UTF_8.withoutParameters());
	}
}
