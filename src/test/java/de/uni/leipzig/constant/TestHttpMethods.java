package de.uni.leipzig.constant;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestHttpMethods {

	@Test
	public void testIsCrud() throws Exception {
		assertTrue(HttpMethods.isCrud(HttpMethods.POST));
		assertTrue(HttpMethods.isCrud(HttpMethods.GET));
		assertTrue(HttpMethods.isCrud(HttpMethods.PUT));
		assertTrue(HttpMethods.isCrud(HttpMethods.DELETE));
		assertFalse(HttpMethods.isCrud(HttpMethods.HEAD));
		assertFalse(HttpMethods.isCrud(HttpMethods.TRACE));
		assertFalse(HttpMethods.isCrud(HttpMethods.OPTIONS));
		assertFalse(HttpMethods.isCrud(HttpMethods.CONNECT));
	}
}
