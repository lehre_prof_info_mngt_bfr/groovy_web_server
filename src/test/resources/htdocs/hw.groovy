#!/usr/bin/env groovy

print('Content-Type:text/html;charset=utf-8')
print('\r\n' * 2)
println('<h1>This is a hello world from groovy over cgi!</h1>')
println('<ul>')
System.env.each {k,v ->
  println("<li>$k = $v</li>")
}
println('</ul>')
if(System.env.containsKey('CONTENT_LENGTH')){
  println('<p>')
  println('<h2>There was a body...</h2>')
  System.in.eachLine {
	  println(it)
  }
  println('</p>')
}
