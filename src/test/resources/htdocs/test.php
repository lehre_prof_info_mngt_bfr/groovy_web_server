#!/bin/php
<?php
// We don't use php-cgi due to it's unpredictable nature 
// it does not run scripts when particular CGI environment variables are present
// so we need to add the Content-Type header by ourself 
  echo 'Content-Type:text/html;charset=utf-8' . PHP_EOL . PHP_EOL;
// Also, parse the CGI query string by ourself. PHP can do this easily:
  parse_str($_SERVER['QUERY_STRING'], $_GET);
  $name = "Unbekannter";
  if(isset($_GET['name'])){
  	$name = $_GET['name'];
  }
// Start with html content
?>
<html>
	<body>
		<h1>
			Hallo <?php echo $name; ?>!
		</h1>
	</body>
</html>  

