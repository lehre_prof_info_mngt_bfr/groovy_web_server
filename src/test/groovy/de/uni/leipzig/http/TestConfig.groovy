package de.uni.leipzig.http

import spock.lang.Specification

class TestConfig extends Specification {

	def 'properties can be set programmatically'(){
		given:
			def cs = Config.getInstance()
			def fixture = new File('test')
		when:
			cs.server.newRoot = fixture 
		then:
			cs.server.newRoot == fixture
	}
	
	def 'properties exist and can be merged'(){
		given:
			def cs = Config.getInstance()
			def inputConfigScript = TestConfig.getResource('/testconfig.groovy')
		when: 
			cs.parse(inputConfigScript)
		then:
			cs.server.root == new File('src/test/resources/htdocs') && cs.server.port == 8080 && cs.server.cgi == ['cgi','exe', 'app', 'groovy', 'php'] as Set && cs.server.myProp == 'prop'
	}
	
}
