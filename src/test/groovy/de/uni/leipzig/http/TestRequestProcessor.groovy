package de.uni.leipzig.http

import spock.lang.IgnoreRest;
import spock.lang.Specification

import com.google.common.net.MediaType

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.constant.HttpStatus
import de.uni.leipzig.handler.FileRequestHandler
import de.uni.leipzig.handler.RequestHandler
import de.uni.leipzig.routing.Routes

class TestRequestProcessor extends Specification {

	def static pathToRoot = 'src/test/resources/htdocs'
	
	static {
		def config = Config.getInstance()
		config.server.root = new File(pathToRoot)
		config.server.timeout = 350
	}
	
	def headerbase = ['Agent': 'test', 
					   Host: 'localhost']
	
	def contentFile = new File(pathToRoot + '/index.html')
	def modificationDate
	def setup(){
		def ts = System.currentTimeMillis()
		contentFile.setLastModified(ts)
		modificationDate = FileRequestHandler.formater.format(ts)
	}
	def sleepTime = Config.getInstance().server.timeout * 2

	def testable = new RequestProcessor()
		
	def 'handle failed request'(){
		given:
			Socket theSocketFailed = Mock()
			def msg = 'status line broken'
			def req = "GET HTTP/1.1"
			def res = Response.builder().protocolVersion(Constants.HTTP_1_1)
						  				.statusCode(HttpStatus.BadRequest)
										.header(['Content-Length': msg.size()])
										.body(msg.getBytes())
										.build()
			def input = new ByteArrayInputStream(req.toString().getBytes())
			def output = new ByteArrayOutputStream()
		when:
			testable.http(theSocketFailed)
			Thread.sleep(sleepTime)
		then:
			1 * theSocketFailed.getInputStream() >> input 
			1 * theSocketFailed.getOutputStream() >> output
			1 * theSocketFailed.close()
			1 * theSocketFailed.getInetAddress() >> Inet4Address.localHost
			res.toString() == new String(output.toByteArray())
	}
	
	def 'handle server error'(){
		given:
			Socket theSocketFailed = Mock()
			def msg = 'my error'
			def body = 'the text to handle!'
			def toHandler = '/toHandler'
			def req = Request.builder().protocolVersion(Constants.HTTP_1_1)
							 .resource(toHandler)
							 .method(HttpMethods.POST)
							 .body(body.getBytes())
							 .header(headerbase).build()
			def handler = Routes.INSTANCE.register(toHandler, HttpMethods.POST, {curReq -> throw new Exception(msg)} as RequestHandler)
			def res = Response.builder().protocolVersion(Constants.HTTP_1_1)
										  .statusCode(HttpStatus.InternalServerError)
										.header(['Content-Length': msg.size()])
										.body(msg.getBytes())
										.build()
			def input = new ByteArrayInputStream(req.toString().getBytes())
			def output = new ByteArrayOutputStream()
		when:
			
			testable.http(theSocketFailed)
			Thread.sleep(sleepTime)
		then:
			println(new String(output.toByteArray()))
			1 * theSocketFailed.getInputStream() >> input
			1 * theSocketFailed.getOutputStream() >> output
			1 * theSocketFailed.close()
			1 * theSocketFailed.getInetAddress() >> Inet4Address.localHost
			res.toString() == new String(output.toByteArray())
	}
	
	def 'handle timeout'(){
		given:
			Socket theSocketTimeout = Mock()
			def msg = Constants.ERROR_MESSAGE_TIMEOUT
			def req = "GET HTTP/1.1"
			def res = Response.builder().protocolVersion(Constants.HTTP_1_1)
										  .statusCode(HttpStatus.RequestTimeOut)
										.header(['Content-Length': msg.size()])
										.body(msg.getBytes())
										.build()
			def input = new InputStream(){
				int read(){1}	
			}
 			def output = new ByteArrayOutputStream()
		when:
			testable.http(theSocketTimeout)
			Thread.sleep(sleepTime)
		then:
			1 * theSocketTimeout.getInputStream() >> {
				input
			}
			1 * theSocketTimeout.getOutputStream() >> {
				output
			}
			1 * theSocketTimeout.close()
			println(new String(output.toByteArray()))
			res.toString() == new String(output.toByteArray())
	}
	
	def 'handle file request only with GET method'(){
		given:
			Socket theSocketGet = Mock()
			def req = createReqForFile('/', HttpMethods.PUT)
			def res = Response.builder().protocolVersion(Constants.HTTP_1_1)
										  .statusCode(HttpStatus.NotFound)
										.header(['Server': 'GroovyWebServer'])
										.body([] as byte[])
										.build()
			def input = new ByteArrayInputStream(req.toString().getBytes())
			def output = new ByteArrayOutputStream()
		when:
			testable.http(theSocketGet)
			Thread.sleep(sleepTime)
		then:
			1 * theSocketGet.getInputStream() >> input
			1 * theSocketGet.getOutputStream() >> output
			1 * theSocketGet.close()
			1 * theSocketGet.getInetAddress() >> Inet4Address.localHost
			res.toString() == new String(output.toByteArray())
	}
	
	def 'handle request response'(){
		given: 
			Socket theSocketNormal = Mock()
			def req = createReqForFile('/', , HttpMethods.GET)
			def input = new ByteArrayInputStream(req.toString().getBytes())
			def output = new ByteArrayOutputStream()
			def body = new File('src/test/resources/htdocs/index.html').getBytes()
			def exp = Response.builder().protocolVersion(Constants.HTTP_1_1)
										 .statusCode(HttpStatus.OK)
										 .header([
											 'Server': 'GroovyWebServer',
											 'Content-Type' : MediaType.HTML_UTF_8.withoutParameters(),
											 'Date' : modificationDate,
											 'Content-Length': body.size(),
											 ])
										 .body(body)
										 .build()
		when: 
			testable.http(theSocketNormal)
			Thread.sleep(sleepTime)
		then: 
			1 * theSocketNormal.getInputStream() >> input 
			1 * theSocketNormal.getOutputStream() >> output
			1 * theSocketNormal.close()
			1 * theSocketNormal.getInetAddress() >> Inet4Address.localHost
			exp.toString() == new String(output.toByteArray()) 
			
	}
	
	private Request createReqForFile(String path, HttpMethods method) {
		def req = Request.builder().protocolVersion(Constants.HTTP_1_1)
				.resource(path)
				.method(method)
				.body([] as byte[])
				.header(headerbase).build()
		return req
	}
	
}
