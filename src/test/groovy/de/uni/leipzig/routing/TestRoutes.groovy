package de.uni.leipzig.routing

import spock.lang.Specification
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.handler.FileRequestHandler
import de.uni.leipzig.handler.HeadRequestHandler;
import de.uni.leipzig.handler.RequestHandler
import de.uni.leipzig.http.Config

class TestRoutes extends Specification {
	static {
		def inst = Config.getInstance()
		inst.server = [ root : new File('src/test/resources/htdocs')]
	}
	
	def testable = Routes.INSTANCE
	
	def 'get default route for get requests'(){
		when:
			def rh = testable.route(HttpMethods.GET, '/myfile.html')
			rh = testable.route(HttpMethods.GET, '/myfile.html')
		then: 
			assert rh instanceof CgiOrFileRequestHandler
	}
	
	def 'get default route for get cgi requests'(){
		when:
			def rh = testable.route(HttpMethods.GET, '/myfile.cgi')
		then:
			assert rh instanceof CgiOrFileRequestHandler
	}
	
	def 'get default route for head requests'(){
		when:
			def rh = testable.route(HttpMethods.HEAD, '/never.html')
		then:
			assert rh instanceof HeadRequestHandler
	}
	
	def 'add route for get request direct match'(){
		given:
			def myRh = {req -> Response.builder().build()} as RequestHandler
		when:
			testable.register('/myfile.html', HttpMethods.GET, myRh)
			def rh = testable.route(HttpMethods.GET, '/myfile.html')
		then:
			rh.is(myRh)
	}
	
}
