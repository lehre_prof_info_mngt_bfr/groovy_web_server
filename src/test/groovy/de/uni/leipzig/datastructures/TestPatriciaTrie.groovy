package de.uni.leipzig.datastructures

import spock.lang.Shared
import spock.lang.Specification

import java.beans.MetaData.java_awt_AWTKeyStroke_PersistenceDelegate;
import java.util.Optional

class TestPatriciaTrie extends Specification {

	def testable = Collections.newSetFromMap(new PatriciaTrie())

	def emptyNodeCan(){
		given:
			def e = Leaf.INSTANCE
		expect:
			e.remove('a') == e.INSTANCE && e.add('test', 123) == new CommonPatriciaNode(key:'test', value : 123)
	}
	
	def words = ['romane', 'romanus', 'romulus', "roe", "romane", "false", "falsify", "false", "romanus"]
	def duplicates = [0,1,5]
	
	@Shared
	def n = new CommonPatriciaNode(key : 'n', value : 2)
	@Shared
	def m = new CommonPatriciaNode(key : 'm', value : 3)
	@Shared
	def s = new CommonPatriciaNode(key : 's', value : 4)
	@Shared
	def t = new CommonPatriciaNode(key : 't', value : 5)
	@Shared
	def x = new CommonPatriciaNode(key : 'x', value : 6)
	@Shared
	def y = new CommonPatriciaNode(key : 'y', value : 7)
	
	@Shared
	def emptyMN = new EmptyPatriciaNode(left : m, right : n)
	@Shared
	def emptyST = new EmptyPatriciaNode(left : s, right : t)
	@Shared
	def emptyMT = new EmptyPatriciaNode(left : m, right : t)
	@Shared
	def emptyNS = new EmptyPatriciaNode(left : n, right : s)
	@Shared
	def emptyXY = new EmptyPatriciaNode(left : x, right : y)
	
	
	def 'trie balance'(){
		when:
			def result = CommonPatriciaNode.balance('ro', 1, left, right)
		then:
			result.left == expectedLeft &&
			result.right == expectedRight &&
			result.key == 'ro' &&
			result.value == 1
		where: 
			left << [m, s, t, 
					 n, Leaf.INSTANCE, emptyMN,
					 emptyMN, emptyMT, emptyST,
					 Leaf.INSTANCE, Leaf.INSTANCE, Leaf.INSTANCE,
					 emptyMT, emptyST]
			right << [s, m, s, 
					  m, Leaf.INSTANCE, s,
					  Leaf.INSTANCE, Leaf.INSTANCE, Leaf.INSTANCE,
					  emptyMN, emptyMT, emptyST,
					  emptyNS, emptyXY]
			expectedLeft << [m, m, Leaf.INSTANCE, 
							 emptyMN, Leaf.INSTANCE,  emptyMN,
							 emptyMN, m, Leaf.INSTANCE,
							  emptyMN, m, Leaf.INSTANCE,
							  emptyMN, Leaf.INSTANCE]
			expectedRight << [s, s, new EmptyPatriciaNode(left : s, right : t), 
							  Leaf.INSTANCE, Leaf.INSTANCE, s,
							  Leaf.INSTANCE, t, emptyST,
							 Leaf.INSTANCE, t, emptyST,
							 emptyST,  EmptyPatriciaNode.linkedList([s,t,x,y])]
	}
	
	def 'trie add same value'(){
		given:
			def testable = new CommonPatriciaNode(key:'romane', value : 1)
		when:
			def result = testable.add('romane', 2)
		then: 
			testable.left == Leaf.INSTANCE && 
			testable.right == Leaf.INSTANCE && 
			testable.key == 'romane' && 
			testable.value == 1 &&
			result.left == Leaf.INSTANCE &&
			result.right == Leaf.INSTANCE &&
			result.key == 'romane' &&
			result.value == 2 &&
			!testable.is(result)
	}
	
	def 'trie add different higher value'(){
		given:
			def testable = new CommonPatriciaNode(key:'romane', value : 1)
		when:
			def result = testable.add('summer', 4)
		then:
			result instanceof EmptyPatriciaNode &&
			result.left == testable &&
			!testable.is(result.left) &&
			result.right == new CommonPatriciaNode(key:'summer', value : 4) 
	}
	
	def 'trie add different lower value'(){
		given:
			def testable = new CommonPatriciaNode(key:'romane', value : 1)
		when:
			def result = testable.add('emit', 4)
		then:
			result instanceof EmptyPatriciaNode &&
			result.left == new CommonPatriciaNode(key:'emit', value : 4) &&
			result.right == testable &&
			!testable.is(result.right) 
	}

	def 'trie add similar smaller value'(){
		when:
			def result = new CommonPatriciaNode(key : 'ryma', value : 1, left : left, right : right).add(addable, value)
		then:
			result.left == expectedLeft &&
			result.right == expectedRight &&
			result.key == addable &&
			result.value == value
		where:
			addable << ['rym','ry', 'r']
			value << [50,51,52]
			left << [n, emptyMN, m]
			right << [s,emptyST, emptyST]
			expectedLeft << [new CommonPatriciaNode(key : 'a', value : 1, right : new EmptyPatriciaNode(left : n, right : s)),
							 new CommonPatriciaNode(key : 'ma', value : 1, left: m, right: EmptyPatriciaNode.linkedList([n, s,t])),
							 Leaf.INSTANCE
							]
			expectedRight << [Leaf.INSTANCE, Leaf.INSTANCE, new CommonPatriciaNode(key : 'yma', value : 1, left: m, right: emptyST),]
	}
	
	def 'trie add similar greater value'(){
		when:
			def result = new CommonPatriciaNode(key : 'roe', value : 1, left : left, right : right).add(addable, value)
		then:
			result.left == expectedLeft &&
			result.right == expectedRight &&
			result.key == 'roe' &&
			result.value == 1
		where:
			addable << ['roet', 'roene', 'roebert']
			value << [5, 10, 100]
			left << [m, m, emptyMN]
			right << [s, s, emptyST]
			expectedLeft << [m, 
							new EmptyPatriciaNode(left : m, right : new CommonPatriciaNode(key : 'ne', value : 10)),
							new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 'bert', value : 100), right : emptyMN)]
			expectedRight << [new EmptyPatriciaNode(left : s, right : t), s, emptyST]
	}
	
	def 'trie add partial similar value'(){
		given:
			def testable = new CommonPatriciaNode(key:'romane', value : 1)
		when:
			def result = testable.add('romanus', 4)
		then:
			!testable.is(result) &&
			result.left == new CommonPatriciaNode(key:'e', value : 1) && 
			result.right == new CommonPatriciaNode(key:'us', value : 4) && 
			result.key == 'roman' && result.value == null
	}
	
	def 'trie add partial similar value problem'(){
		given:
			def testable = new CommonPatriciaNode(key:'romant', value : 1, left : emptyMN)
		when:
			def result = testable.add('romanus', 4)
		then:
			!testable.is(result) &&
			result.left == Leaf.INSTANCE && 
			result.right == new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 't', value : 1, left : emptyMN),
												      right : new CommonPatriciaNode(key : 'us', value : 4)) &&
			result.key == 'roman' && result.value == null
	}

	def 'trie add nested partial similar value'(){
		given:
			def e = new CommonPatriciaNode(key : 'e', value : 1)
			def us = new CommonPatriciaNode(key : 'us', value : 2)
			def testable = new CommonPatriciaNode(key:'roman', value : null, left : e, right : us)
		when:
			def result = testable.add('romandi', 4)
			result = result.add('romanusi', 5)
		then:
			result.left == new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 'di', value : 4), right : e) && 
			result.right == us.add('usi', 5)  
	}
		
	def 'trie add similar values'(){
		given:
			def testable = new CommonPatriciaNode(key:'romane', value : 1)
		when:
			def result = testable.add('romanus', 2)
			result = result.add('romulus', 3)
		then:
			result == new CommonPatriciaNode(key : 'rom', value : null, 
							left : new CommonPatriciaNode(key : 'an', value: null, right : new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 'e', value : 1), right : new CommonPatriciaNode(key : 'us', value : 2))), 
							right : new CommonPatriciaNode(key : 'ulus', value: 3))
	}
	
	def 'trie iterate'(){
		given:
			def testable = Leaf.INSTANCE
			def expected = [words, (0..words.size()).toList()].transpose()
											  				  .collect{[it[0], it[1]] as Tuple2}
															  .findAll{
																  !duplicates.contains(it.getSecond())
																  }
															  .sort {it.getFirst()}
		when:
			words.eachWithIndex {item, index -> testable = testable.add(item, index)}
		then: 
			expected == testable.iterate()
		
	}
	
	def 'trie add toString'(){
		given:
			def testable = Leaf.INSTANCE
		when:
			words.eachWithIndex {item, index -> testable = testable.add(item, index)}
			testable = testable.add("romane", 1090)
		then:
			testable.toString() == "''('fals'('e'(<E>,<E>),'ify'(<E>,<E>)),'ro'(''('e'(<E>,<E>),'m'('an'(<E>,''('e'(<E>,<E>),'us'(<E>,<E>))),'ulus'(<E>,<E>))),<E>))"
	}
	
	def 'trie search'(){
		given:
			def testable = Leaf.INSTANCE
			words.eachWithIndex {item, index -> testable = testable.add(item, index)}
		expect:
			words.eachWithIndex {item, index ->
				if(!duplicates.contains(index)) 
					assert testable.search(item).get() == index
			}
	}
	
	def 'empty node remove'(){
		given: 
			def testable = new EmptyPatriciaNode(left : l, right : r)
		when:
			def result = testable.remove(key)
		then:
			expected == result
		where:
			l << [Leaf.INSTANCE, s, m]
			r << [s, Leaf.INSTANCE, n]
			key << ["m", "t", "s"]
			expected << [s, s, emptyMN]
		
	}
	
	def 'normal node remove'(){
		given:
			def testable = new CommonPatriciaNode(key : fixture.key, value: fixture.value, left: l, right: r)
		when:
			def result = testable.remove(removable)
		then:
			expected == result
		where:
			fixture << [m, s, t,
						s, s, s.add('s', null)]
			removable << [m.key, s.key, 'asdf', 
						  s.key + m.key, s.key + t.key, , s.key + m.key]
			l << [Leaf.INSTANCE, m, s, 
				  m, m, m]
			r << [Leaf.INSTANCE, t, x,
				  t, t, Leaf.INSTANCE]
			expected << [Leaf.INSTANCE, new CommonPatriciaNode(key : s.key, value : null, left: m, right : t), new CommonPatriciaNode(key : t.key, value : t.value, left : s, right : x), 
						 new CommonPatriciaNode(key : s.key, value : s.value, left: Leaf.INSTANCE, right : t), new CommonPatriciaNode(key : s.key, value : s.value, left: m, right : Leaf.INSTANCE), Leaf.INSTANCE]
		
	}
	
	def 'trie remove'(){
		given:
			def words = ['romane', 'romanus', 'romulus', "roe", "romane", "false", "falsify", "false", "romanus"]
			def testable = Leaf.INSTANCE
			words.eachWithIndex {item, index -> testable = testable.add(item, index)}
			testable = testable.remove('romane')
		expect:
			assert testable.search('romane') == java.util.Optional.empty()
	}

	def 'emtpy node equals'(){
		given:
			def left = new CommonPatriciaNode(key : 'abc', value : 1)
			def testable = new EmptyPatriciaNode(left : left, right : m)
		when:
			assert(testable.left == left)
			def result = testable.add("acd", 2)
		then:
			!testable.is(result) &&
			result.left == new CommonPatriciaNode(key : 'a', value : null, 
												left : Leaf.INSTANCE,
												right : new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 'bc', value : 1), right : new CommonPatriciaNode(key : 'cd', value : 2))) && 
			result.right == m &&
			result instanceof EmptyPatriciaNode && result.key == '' && result.value == null
	}

	def 'emtpy node add greater'(){
		given: 
			def left = new CommonPatriciaNode(key : 'abc', value : 1)
			def testable = new EmptyPatriciaNode(left : left, right : m)
		when:
			assert(testable.left == left) 
			def result = testable.add("bcd", 2)
		then:
			result.left == left && 
			result.right == new EmptyPatriciaNode(left : new CommonPatriciaNode(key : 'bcd', value : 2), right : m) && 
			result.key == '' && result.value == null
			
	}
	
	def 'emtpy node add lower'(){
		given:
			def left = new CommonPatriciaNode(key : 'bcd', value : 1)
			def testable = new EmptyPatriciaNode(left : left, right : m)
		when:
			assert(testable.left == left)
			def result = testable.add("abc", 2)
		then:
			result.left == new CommonPatriciaNode(key : 'abc', value : 2) &&
			result.right == new EmptyPatriciaNode(left : left, right : m) &&
			result.key == '' && result.value == null
	}
	
	def 'tree difference'(){
		expect:
			def diff = StringDifference.difference(s1, s2)
			assert diff == expected
		where:
			s1 << ['romane', 'romulus', 'rom', 'rome', 'rom', 'rom', 'roe']	
			s2 << ['romanus', 'romane', 'rom', 'rom', 'romulus', 'tom', 'rom']
			expected << [new PartlyEqual(countOfEqualLetters : 5, equalPart: 'roman', notEqualPart: 'us', notEqualPartKey : 'e') , 
						 new PartlyEqual(countOfEqualLetters : 3, equalPart: 'rom', notEqualPart: 'ane', notEqualPartKey : 'ulus'),
						 new Equal(3, 'rom'), 
						 new SmallerCurrentNode(countOfEqualLetters : 3, equalPart: 'rom', notEqualPart : 'e'), 
						 new GreaterCurrentNode(countOfEqualLetters : 3, equalPart: 'rom', notEqualPart : 'ulus'), 
						 new NotEqual(),
						 new PartlyEqual(countOfEqualLetters : 2, equalPart: 'ro', notEqualPart: 'm', notEqualPartKey : 'e'),
						 ]
	}
	
	
}

