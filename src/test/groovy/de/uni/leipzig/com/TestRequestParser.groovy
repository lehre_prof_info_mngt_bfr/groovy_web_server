package de.uni.leipzig.com

import static de.uni.leipzig.com.RequestParser.BUFFER_SIZE
import static de.uni.leipzig.constant.Constants.CRLF
import static de.uni.leipzig.constant.Constants.CONTENT_LENGTH
import spock.lang.IgnoreRest
import spock.lang.Specification
import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.exception.SemanticRequestParseException
import de.uni.leipzig.exception.SyntaxRequestParseException

class TestRequestParser extends Specification {

	def host = 'theHost'
	
	def 'request parser works'(){
		given:
			def body = "test"
			def con = CONTENT_LENGTH 
			def input = Request.builder().protocolVersion("HTTP/1.1")
				.resource("/")
				.method(HttpMethods.GET)
				.body(body.getBytes())
				.header([Host : host,
						 "User-Agent" : "myAgent",
						 "Content-Length" : body.length() // the type is int here!!!
						]).build()
			def testable = new RequestParser(new ByteArrayInputStream(input.toString().getBytes()))
		when:
			def result = testable.parse() 
		then:
			input == result
	}
	
	def 'body is not parsed in the abesence of content-length'(){
		given:
			def body = "test"
			def input = Request.builder().protocolVersion("HTTP/1.1")
				.resource("/")
				.method(HttpMethods.POST)
				.body(body.getBytes())
				.header([Host : host,
						"User-Agent" : "agent"]).build()
			def testable = new RequestParser(new ByteArrayInputStream(input.toString().getBytes()))
		when:
			def result = testable.parse()
		then:
			body != new String(result.getBody())
	}
	
	def 'request parser throw exception when multiple content length values are given'(){
		given:
			def input = """POST / HTTP/1.1
						   |User-Agent: asdf
						   |Host : host
						   |Content-Length: 3
						   |Content-Length: 4
						   |
						   |1234
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			def r = testable.parse()
		then:
			thrown SemanticRequestParseException
	}
	
	def 'request parser throw exception when body is not well formated'(){
		given:
			def input = """POST / HTTP/1.1
						   |User-Agent: asdf
						   |Host: host
						   |Content-Length: 3
						   |
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			testable.parse()
		then:
			thrown SyntaxRequestParseException
	}
	
	def 'request parser throw exception when header is not well formated'(){
		given:
			def input = """POST / HTTP/1.1
						   |User-Agent asdf
						   |
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			testable.parse()
		then:
			thrown SyntaxRequestParseException
	}
	
	def 'request parser throw exception when header does not contain host information'(){
		given:
			def input = """POST / HTTP/1.1
						   |User-Agent: asdf
						   |
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			testable.parse()
		then:
			thrown SemanticRequestParseException
	}
	
	def 'request parser throw exception when method is broken'(){
		given: 
			def input = """corruptMethod / HTTP/1.1
						   |User-Agent: asdf
						   |
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			testable.parse()
		then:
			thrown SyntaxRequestParseException
	}
	
	def 'request parser throw exception when status line is broken'(){
		given:
			def input = """POST /
						   |User-Agent: asdf
						   |
""".stripMargin().replaceAll('\n', '\r\n')
			def testable = new RequestParser(new ByteArrayInputStream(input.getBytes()))
		when:
			testable.parse()
		then:
			thrown SyntaxRequestParseException
	}
	
	def 'getNextLine with empty string'(){
		given:
			def input = ''
		when:
			def result = RequestParser.getNextLine(new ByteArrayInputStream(input.getBytes()))
		then:
			result.size() == 0 && result.equals(input)
	}
	
	def 'input stream buffer size'(){
		given:
			RequestParser.BUFFER_SIZE = 4
			def input = 'asdf' * 2
		when:
			def result = RequestParser.getNextLine(new ByteArrayInputStream(input.getBytes()))
		then:
			result.size() == 8 && result.equals(input)
	}
	
	def 'input stream next line'(){
		given:
			def input = """line one
					   |
					   |lineThree
				""".stripMargin().replaceAll('\n', CRLF)
			def inputAsBytes = input.getBytes()
			def inputAsStream = new ByteArrayInputStream(inputAsBytes)
		expect:
			String lineOne = RequestParser.getNextLine(inputAsStream)
			String lineTwo = RequestParser.getNextLine(inputAsStream)
			String lineThree = RequestParser.getNextLine(inputAsStream)
			assert 'line one' == lineOne
			assert '' == lineTwo
			assert 'lineThree' == lineThree
	}
}
