package de.uni.leipzig.com

import spock.lang.Specification

import com.google.common.net.MediaType

import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus

class TestResponse extends Specification {

	def 'Response is buildable using a builder and status line construction is possible'(){
		given:
			def protocolVersion = "HTTP/1.1"
			def status = HttpStatus.OK
			HttpMessage testable = Response.builder().protocolVersion(protocolVersion)
												     .statusCode(status)
													 .build()
		expect:
			"${protocolVersion} 200 OK" == testable.getFirstLine()
	}										

	def protocolVersion = "HTTP/1.1"
	def status = HttpStatus.OK
	def testable = Response.builder().protocolVersion(protocolVersion)
										.statusCode(status)
									 .header(["Accept":MediaType.TEXT_JAVASCRIPT_UTF_8.withoutParameters()])
									 .body([] as byte[])
									 .build()
									 
	def expectedStringRepresentation = """${protocolVersion} ${status}
							 			  |Accept: text/javascript
							 			  |
""".stripMargin().replaceAll('\n', Constants.CRLF)
														
	def 'Response is buildable using a builder and toString works'(){
		expect : 
			testable.toString() == expectedStringRepresentation
	}
	
	def 'Response can be written to a stream'(){
		given:
			def out = new ByteArrayOutputStream()
		when:
			testable.to(out)
		then:
			expectedStringRepresentation == new String(out.toByteArray())
	}
}
