package de.uni.leipzig.com

import java.lang.reflect.Method
import java.util.Map

import org.junit.rules.ExpectedException;

import com.google.common.net.MediaType

import de.uni.leipzig.constant.HttpMethods;
import spock.lang.Specification
import static de.uni.leipzig.constant.Constants.CRLF
import static de.uni.leipzig.constant.Constants.UTF_8

class TestRequest extends Specification {

	def protocolVersion = "http/1.1"
	def resource = "/"
	def method = HttpMethods.GET
	def body = [33,33,33] as byte[]
	def header = ["User-Agent" : "other",
				  "MyOtherProp" : "ok",
				  "Content-Length" : body.length]
	def testable = createRequest(method, resource, protocolVersion, body, header)
	
	def 'parse Request Line'(){
		given :
			def mediaType = MediaType.TEXT_JAVASCRIPT_UTF_8.withoutParameters()
			def method = HttpMethods.POST
			def agentName = "TheGreatAgent"
			def body = "Hello World!"
			def path = "/"
			def header = ["Content-Length": body.length(), 
						  "Accept" : mediaType.toString(),
						  "User-Agent": agentName,
						  Host : 'myHost']
			def httpRequest = """${method} ${path} ${protocolVersion}
						    |${createHeaderString(header)}
							|${body}""".stripMargin().replaceAll('\n', CRLF)
			byte[] inputData = httpRequest.getBytes(UTF_8)
		when:
			def result = Request.fromInputSteam(new ByteArrayInputStream(inputData)) 
		then: 
			def expected = createRequest(method, path, protocolVersion, body.getBytes(UTF_8), header)
			expected == result
	}
	
	def 'equals works'(){
		given :
			def testable2 = createRequest(method, resource, protocolVersion, body, header)
			def testable3 = createRequest(method, resource, "asdf", body, header)
		expect:
			testable.equals(testable2) &&
			!testable.equals(testable3)
	}
	
	def 'test getters can be used'(){
		expect :
			method.equals(testable.getMethod())
			
	}
	
	def "setters can't be used"(){
		when: 
			testable.setMethod(HttpMethods.POST)
		then: 
			thrown MissingMethodException
	}

	
	def 'test to String with empty body'(){
		given :
			def body = [] as byte[]
			def testable = createRequest(method, resource, protocolVersion, body, header)
			def expected = createExpected(method, resource, protocolVersion, body, header)
		expect:
			expected == testable.toString()
	}
	
	def 'test to String with empty header'(){
		given :
			def body = [] as byte[]
			def header = [:] 
			def testable = createRequest(method, resource, protocolVersion, body, header)
			def expected = createExpected(method, resource, protocolVersion, body, header)
		expect:
			expected == testable.toString()
	}
	
	def 'test to String creates a valid http request'(){
		given :
			def expected = createExpected(method, resource, protocolVersion, body, header)
		expect:
			expected == testable.toString()
	}

	private Request createRequest(HttpMethods method, String resource, String protocolVersion, byte[] body, Map header) {
		def testable = Request.builder().protocolVersion(protocolVersion)
				.resource(resource)
				.method(method)
				.body(body)
				.header(header).build()
		return testable
	}

	private String createExpected(HttpMethods method, String resource, String protocolVersion, byte[] body, Map header) {
		def expected = """${method.toString()} ${resource} ${protocolVersion}
							|${createHeaderString(header)}
							|${new String(body)}""".stripMargin().replaceAll('\n', CRLF)
		return expected
	}
	
	def createHeaderString(header){
		if(header.isEmpty()){
			""
		} else {
			header.collect{ k,v ->
				"${k}: ${v}" 
			}.join(CRLF) + CRLF
		}
	}
}
