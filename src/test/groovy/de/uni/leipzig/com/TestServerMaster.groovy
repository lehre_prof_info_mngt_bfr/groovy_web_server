package de.uni.leipzig.com

import spock.lang.Specification
import de.uni.leipzig.handler.TestFileRequestHandler
import de.uni.leipzig.http.Config

class TestServerMaster extends Specification {

	def 'test server listening on port 8085'(){
		given: 
			def port = 8085
			def config = Config.getInstance()
			config.server.root = new File('src/test/resources/htdocs')
			config.server.port = port
			def req = TestFileRequestHandler.createReqForFile('/')
			def testable = new ServerMaster()
		when:
			testable.start()
			Thread.sleep(50)
			Socket s = new Socket(Inet4Address.localHost, port)
			def os = s.getOutputStream()
			req.to(os) 
		then:
			Thread.sleep(1000)
			testable.stop()
			
	}
	
}
