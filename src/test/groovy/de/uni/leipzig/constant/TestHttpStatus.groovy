package de.uni.leipzig.constant

import de.uni.leipzig.exception.SemanticRequestParseException;
import de.uni.leipzig.exception.SyntaxRequestParseException;
import spock.lang.Specification

class TestHttpStatus extends Specification {

	def 'test get status from int'(){
		expect:
			HttpStatus.Continue == HttpStatus.from(100)
	}
	
	def 'test get status from String'(){
		expect:
			HttpStatus.Continue == HttpStatus.from('100')
	}
	
	def 'test get status from bad String'(){
		when:
			HttpStatus.from('100asdf')
		then: 
			thrown SyntaxRequestParseException
	}
	
	def 'test status code is known'(){
		when: 
			HttpStatus.from(99)
		then:
			thrown SemanticRequestParseException
	}
}
