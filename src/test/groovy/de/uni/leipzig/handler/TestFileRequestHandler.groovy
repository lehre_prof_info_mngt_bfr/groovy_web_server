package de.uni.leipzig.handler

import spock.lang.Specification

import com.google.common.net.MediaType

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.constant.HttpStatus

class TestFileRequestHandler extends Specification {

	static final String HTTP_1_1 = "HTTP/1.1"
	
	def pathToRoot = 'src/test/resources/htdocs'
	def root = new File(pathToRoot)
	RequestHandler fileHandler = new FileRequestHandler(root)
	def contentFile = new File(pathToRoot + '/index.html')
	def txtFile = new File(pathToRoot + '/thefile.txt')
	def modificationDate
	def setup(){
		def ts = System.currentTimeMillis()
		contentFile.setLastModified(ts)
		txtFile.setLastModified(ts)
		modificationDate = FileRequestHandler.formater.format(ts)
	}
	def content = contentFile.getBytes()
	
	def 'handle file extraction properly'(){
		given: 	
			def req = createReqForFile("/thefile.txt")
			def expected = Response.builder().protocolVersion(HTTP_1_1)
									  .statusCode(HttpStatus.OK)
									  .header([	'Server' : "GroovyWebServer",
 										 		"Content-Type"  : MediaType.PLAIN_TEXT_UTF_8.withoutParameters(),
 										 		'Date' : modificationDate,
												'Content-Length': 4,
									   ])
									  .body('asdf'.getBytes())
									  .build()
		when:
			def res = fileHandler.handleRequest(req) 
		then: 
			expected == res
			
	}
	
	def 'handle content type extraction properly'(){
		given:
			def req = createReqForFile("http://host:80/index.html")
			def expected = Response.builder().protocolVersion(HTTP_1_1)
									  .statusCode(HttpStatus.OK)
									  .header([	'Server' : "GroovyWebServer",
												  "Content-Type":MediaType.HTML_UTF_8.withoutParameters(),
												  'Content-Length': content.size(),
												  'Date' : modificationDate]
									  ).body(content)
									  .build()
		when:
			def res = fileHandler.handleRequest(req)
		then:
			expected == res
			
	}
	
	def 'handle index.html == / extraction properly'(){
		given:
			def req = createReqForFile("/")
			def expected = Response.builder().protocolVersion(HTTP_1_1)
									  .statusCode(HttpStatus.OK)
									  .header([	'Server' : "GroovyWebServer",
												"Content-Type":MediaType.HTML_UTF_8.withoutParameters(),
												'Content-Length' : content.size(),
												'Date' : modificationDate]
									  ).body(new File(pathToRoot + '/index.html').getBytes())
									  .build()
		when:
			def res = fileHandler.handleRequest(req)
		then:
			expected == res
			
	}
	
	def 'handle empty folder / extraction properly'(){
		given:
			def req = createReqForFile("/emptyFolder")
		when:
			def res = fileHandler.handleRequest(req)
		then:
			notFound == res
			
	}
	
	def notFound = Response.builder().protocolVersion(HTTP_1_1)
										.statusCode(HttpStatus.NotFound)
										.header(['Server' : "GroovyWebServer"])
										.body([] as byte[])
										.build()
	
	def 'handle non existing file properly'(){
		given:
			def req = createReqForFile("http://host:80/idonotexist.txt")
			
		when:
			def res = fileHandler.handleRequest(req)
		then:
			notFound == res
			
	}


	public static Request createReqForFile(String path) {
		def req = Request.builder().protocolVersion(HTTP_1_1)
				.resource(path)
				.method(HttpMethods.GET)
				.body([] as byte[])
				.header(['Agent': 'test']).build()
		return req
	}
}
