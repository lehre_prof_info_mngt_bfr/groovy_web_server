package de.uni.leipzig.constant;

public enum HttpMethods {

	POST,
	GET,
	PUT,
	DELETE,
	
	HEAD,
	OPTIONS,
	TRACE,
	CONNECT;

	public static boolean isCrud(HttpMethods method) {
		boolean isCrud = false;
		switch(method){
			case POST:
			case GET:
			case PUT: 
			case DELETE:
				isCrud = true;
		}
		return isCrud;
	}
	
}
