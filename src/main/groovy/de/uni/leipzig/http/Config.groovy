package de.uni.leipzig.http

import de.uni.leipzig.constant.Constants;

class Config {
	private static Config INSTANCE
	
	static synchronized Config getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Config()
		} 
		INSTANCE
	} 
	
	@Delegate 
	ConfigObject config

	private Config(){
		this.config = new ConfigObject() 
		config.server = ['root' : new File(Constants.ROOT_DEFAULT), 
						 'timeout' : Constants.TIMEOUT,
						 'port' : 8081,
						 'cgi' : ['cgi','exe', 'app', 'groovy', 'php'] as Set,
						 'phpexe' : 'php'] 
	}
	
	def parse(URL toResource){
		merge(new ConfigSlurper().parse(toResource)) 
	}	
	
	@Override
	public String toString() {
		config.server.collect{k,v ->
			"${k}: ${v}"
		}.join("\n")
	}
}
