package de.uni.leipzig.http

import groovy.transform.InheritConstructors
import groovyx.gpars.actor.Actor
import groovyx.gpars.actor.DefaultActor
import de.uni.leipzig.com.Request
import de.uni.leipzig.com.RequestParser
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus
import de.uni.leipzig.exception.RequestParseException
import de.uni.leipzig.handler.RequestHandler
import de.uni.leipzig.routing.Routes

abstract class HttpActor extends DefaultActor {
	
	protected Socket theSocket
	protected Actor target
	
	HttpActor(Socket theSocket, Actor target){
		this.theSocket = theSocket
		this.target = target
	}
	
	def afterStop(List undeliveredMessages) {
		println('stop actor ' + this)
	}
	
	public void onException(Throwable e){
		println('exception ' + this)
		e.printStackTrace()
	}
}

@InheritConstructors
class RequestActor extends HttpActor {
	
	@Override
	protected void act() {
		react { msg ->
			def req = new RequestParser(new BufferedInputStream(theSocket.getInputStream())).parse()
			println("ok wir haben einen request zerlegt: ${req}")
			target.send(req)
		}
	}
	
	public void onException(Throwable e){
		super.onException(e)
		target.send(e)
	}
	
}

@InheritConstructors
class ResponseActor extends HttpActor {
	
	@Override
	protected void act() {
		react(Config.INSTANCE.server.timeout) { msg ->
			def res
			println("ok wir sind im ResponseActor mit: \n${msg}")
			switch(msg){
				case Actor.TIMEOUT:
					println('Got timeout...')
						res = buildErrorMsgResponse(Constants.ERROR_MESSAGE_TIMEOUT, HttpStatus.RequestTimeOut)
					break;
				case RequestParseException:
					println('Got request parse exception...')
						res = buildErrorMsgResponse(msg.getMessage(), HttpStatus.BadRequest)
					break;
				case Exception:
					println('Got exception...')
						res = buildErrorMsgResponse(msg.getMessage(), HttpStatus.InternalServerError)
					break;
				case Request:
					println('There is a request...')
					Request req = msg
					RequestHandler reqHandler = Routes.INSTANCE.route(req.getMethod(), req.getResource())
					res = reqHandler.handleRequest(req)
					break;
			}
			res.to(new BufferedOutputStream(theSocket.getOutputStream()))
			sendToTarget(res)
		}
		
	}
	
	public void sendToTarget(sendable){
		def tuple = [sendable, theSocket] as Tuple2
		target << tuple 
	}
	
	public void onException(Throwable e){
		super.onException(e)
		println('Got exception...')
		def res = buildErrorMsgResponse(e?.getMessage() ?: '' , HttpStatus.InternalServerError)
		res.to(new BufferedOutputStream(theSocket.getOutputStream()))
		sendToTarget(res)
	}
	
	def buildErrorMsgResponse(String msg, HttpStatus status) {
		Response.builder().protocolVersion(Constants.HTTP_1_1)
						  .statusCode(status)
								 .header(['Content-Length': msg.getBytes().size()])
								 .body(msg.getBytes())
								 .build()
	}
	
}
	
class RequestProcessor extends DefaultActor {

	RequestProcessor(){
		start()
	}
	
	@Override
	protected void act() {
		loop {
			react() { msg ->
				def first = msg.getFirst()
				def socket = msg.getSecond()
				switch(first){
					case Response:
						println("Log response to ${socket.getInetAddress()}:\n" + msg)
						break;
				}
				println("Closing connection to ${socket}...")
				socket.close() // keep alive?!
			}
		}
	}
	
	def http(Socket theSocket){
		println('wir haben einen socket, los gehts!')
		def resa = new ResponseActor(theSocket,this)
		def reqa = new RequestActor(theSocket,resa)
		resa.start()
		reqa.start()
		reqa << "Los gehts!"
	}
	
}
