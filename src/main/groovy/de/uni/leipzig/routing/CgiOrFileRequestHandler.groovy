package de.uni.leipzig.routing

import groovy.transform.InheritConstructors
import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.handler.AbstractRequestHandler
import de.uni.leipzig.handler.CgiRequestHandler
import de.uni.leipzig.handler.FileRequestHandler
import de.uni.leipzig.http.Config

@InheritConstructors
class CgiOrFileRequestHandler extends AbstractRequestHandler {

	@Override
	public Response handleFileRequest(Request req, File local, Map header) {
		def ext = getFileExtention(local.getAbsolutePath())
		def config = Config.getInstance()
		if(config.server.cgi.contains(ext)){
			new CgiRequestHandler(config.server.root).handleFileRequest(req, local, header)
		} else {
			new FileRequestHandler(config.server.root).handleFileRequest(req, local, header)
		}
	}
}
