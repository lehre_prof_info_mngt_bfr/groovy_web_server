package de.uni.leipzig.routing

import static de.uni.leipzig.constant.HttpMethods.*
import groovy.transform.InheritConstructors

import java.util.regex.Pattern

import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.datastructures.PatriciaTrie
import de.uni.leipzig.handler.CgiRequestHandler
import de.uni.leipzig.handler.FileRequestHandler
import de.uni.leipzig.handler.HeadRequestHandler
import de.uni.leipzig.handler.NotFoundRequestHandler
import de.uni.leipzig.handler.RequestHandler
import de.uni.leipzig.http.Config

@InheritConstructors
class NoMatchingRouteException extends Exception {}

enum Routes {
	
	INSTANCE
	
	Map<String, Map<HttpMethods, RequestHandler>> routes = [:] as PatriciaTrie
	
	HashMap<HttpMethods, ArrayDeque<Tuple2<Pattern, RequestHandler>>> routesInternal = [:]
	
	private Routes(){
		def config = Config.getInstance()
		def serverRoot = config.server.root
		def m = [
			(HEAD) : new HeadRequestHandler(serverRoot),
			(GET) : new CgiOrFileRequestHandler(serverRoot),
			(POST) : new CgiRequestHandler(serverRoot),	
			(PUT) : new CgiRequestHandler(serverRoot),
			(DELETE) : new CgiRequestHandler(serverRoot)
		]
		routes = routes.withDefault {
			addDefaultRequestHandler(m) 
		} 
	}
	
	RequestHandler route(HttpMethods method, String resource) {
		def methodToRhMapping = routes.get(resource)
		def requestHandler = methodToRhMapping.get(method)
		requestHandler
	}
	
	def register(String resource, HttpMethods m, RequestHandler h) {
		def mapping = routes.get(resource) == null ?: addDefaultRequestHandler([:])
		mapping.put(m, h)
		routes.put(resource, mapping)
	}

	private addDefaultRequestHandler(map) {
		map.withDefault {new NotFoundRequestHandler()}
	}
	
}
