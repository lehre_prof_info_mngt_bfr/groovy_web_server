package de.uni.leipzig.handler

import groovy.transform.InheritConstructors

import java.io.File;
import java.text.SimpleDateFormat
import java.util.Map;

import com.google.common.net.MediaType

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus

@InheritConstructors
class HeadRequestHandler extends AbstractRequestHandler {
	
	Response handleFileRequest(Request req, File local, Map header){
		okFile(local, header, getResponseBase())
	}
	
	static SimpleDateFormat formater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z") // RFC-822
	
	protected okFile(local, header, res) {
		def fileExtention = getFileExtention(local.getPath())
		header.put(Constants.CONTENT_TYPE, extentionMapping.get(fileExtention))
		header.put(Constants.DATE, formater.format(local.lastModified()))
		def bytesOfFile = local.getBytes()
		header.put(Constants.CONTENT_LENGTH, bytesOfFile.size())
		res.statusCode(HttpStatus.OK)
		   .body([] as byte[])
		   .header(header)
		   .build()
	}

}
