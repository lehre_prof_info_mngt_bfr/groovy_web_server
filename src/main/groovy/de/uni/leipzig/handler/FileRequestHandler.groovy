package de.uni.leipzig.handler

import java.text.SimpleDateFormat;

import com.google.common.net.MediaType

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus
import groovy.transform.InheritConstructors;

@InheritConstructors
class FileRequestHandler extends HeadRequestHandler {
	
	protected Object okFile(local, header, res) {
		super.okFile(local, header, res)
		def bytesOfFile = local.getBytes()
		res.body(bytesOfFile)
		   .build()
	}
	
}
