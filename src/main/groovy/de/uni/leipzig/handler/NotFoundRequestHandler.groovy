package de.uni.leipzig.handler

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus

class NotFoundRequestHandler implements RequestHandler {

	public Response handleRequest(Request req) {
		Response.builder().protocolVersion(Constants.HTTP_1_1)
						  .statusCode(HttpStatus.NotFound)
						  .header(['Server': 'GroovyWebServer'])
						  .body([] as byte[])
						  .build()
	}

}
