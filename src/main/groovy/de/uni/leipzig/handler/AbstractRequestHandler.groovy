package de.uni.leipzig.handler

import com.google.common.net.MediaType

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus

abstract class AbstractRequestHandler implements RequestHandler {
	
	protected static Map<String, MediaType> extentionMapping = [
		'txt': MediaType.PLAIN_TEXT_UTF_8.withoutParameters(),
		'html': MediaType.HTML_UTF_8.withoutParameters(),
		'json': MediaType.JSON_UTF_8.withoutParameters(),
		'xml': MediaType.XML_UTF_8.withoutParameters(),
		'js': MediaType.JAVASCRIPT_UTF_8.withoutParameters(),
		'css': MediaType.CSS_UTF_8.withoutParameters(),
	].withDefault {MediaType.OCTET_STREAM}

	protected File root
		
	AbstractRequestHandler(String root){
		this.root = new File(root)
	}
	
	AbstractRequestHandler(File root){
		this.root = root
	}
	
	public Response handleRequest(Request req) {
		def resPath = URI.create(req.getResource())
		def path = resPath.getPath()
		def extention = path.split(/\./)
		def local 
		if(extention.size() > 1 && extention[1].contains('/')){
			local = new File("${root.getPath()}/${extention[0]}.${extention[1].split(/\//)[0]}")
		} else {
			local = new File("${root.getPath()}/${path}")
		}
		
		def header = ['Server' : "GroovyWebServer"]
		if(local.exists()){
			if(local.isDirectory()){
				checkForIndexfile(req, local, header)
			} else {
				handleFileRequest(req, local, header)
			}
		} else {
			notFound(header)
		}
		
	}

	abstract Response handleFileRequest(Request req, File local, Map header)
	
	private Response checkForIndexfile(req, local, header) {
		def indexFileList = local.listFiles({File f -> f.getName().contains('index')} as FileFilter)
		def returnable
		switch(indexFileList.size()){
			case 0:
				returnable = notFound(header)
				break
			case 1: 
				returnable = handleFileRequest(req, indexFileList[0], header)
				break
			default:
				returnable = notFound(header)
		}
		returnable
	}
	
	protected String getFileExtention(String path){
		def split = path.split(/\./)
		split[split.size() - 1]
	}
	
	protected notFound(header) {
		getResponseBase().header(header)
						 .statusCode(HttpStatus.NotFound)
						 .body([] as byte[])
						 .build()
	}
	
	protected def getResponseBase(){
		Response.builder().protocolVersion(Constants.HTTP_1_1)
	}
	
}
