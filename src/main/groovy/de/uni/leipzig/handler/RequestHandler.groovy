package de.uni.leipzig.handler

import de.uni.leipzig.com.Request
import de.uni.leipzig.com.Response

trait RequestHandler {

	abstract Response handleRequest(Request req);
}
