package de.uni.leipzig.handler 

import groovy.transform.InheritConstructors
import de.uni.leipzig.com.Request
import de.uni.leipzig.com.RequestParser
import de.uni.leipzig.com.Response
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpStatus
import de.uni.leipzig.http.Config

public enum OS {
	WINDOWS, LINUX, MAC, SOLARIS
};// Operating systems.

class OsResolver {
	static def os;
	
	static def synchronized getOs(){
		if(os == null){
			os = resolveOs()
		} 
		os
	}

	static OS resolveOs() {
		String operSys = System.getProperty("os.name").toLowerCase();
		if (operSys.contains("win")) {
			OS.WINDOWS;
		} else if (operSys.contains("nix") || operSys.contains("nux")
		|| operSys.contains("aix")) {
			OS.LINUX;
		} else if (operSys.contains("mac")) {
			OS.MAC;
		} else if (operSys.contains("sunos")) {
			OS.SOLARIS;
		}
	}
}

@InheritConstructors
class CgiRequestHandler extends AbstractRequestHandler {

	Set supportedExtentions = Config.getInstance().server.cgi
	
	@Override
	Response handleFileRequest(Request req, File local, Map header){
		def res = getResponseBase()
		def proc
		def extention = getFileExtention(local.getName())
		if(supportedExtentions.contains(extention)){ // the web server is able to execute the file, but his DOES NOT determine if the file is executable itself
			def cmd = createCmd(local, extention)
			def cgiEnv = createEnv(req, local, extention)
			proc = cmd.execute(cgiEnv, local.getParentFile())
			if(proc){
				forwardReqBodyToCgiViaStdIo(proc, req)
				if(proc.waitFor() == 0){
					def body = handleIoOfCgiScript(proc, req, header)
					header.put(Constants.CONTENT_LENGTH, body.size())
					res.header(header)
					   .statusCode(HttpStatus.OK)
					   .body(body)
					   .build()
				} else {	
					System.err.println(proc.getErrorStream().text)
					System.err.println(proc.getInputStream().text)
					getResponseBase().header(header)
									 .body([] as byte[])
  									 .statusCode(HttpStatus.InternalServerError)
									 .build()
				}
			} else {
				notFound(header)
			}
		} else {
			notFound(header)
		}
		
	}

	private String createCmd(File local, String extention) {
		def cmd = local.getAbsolutePath()
		if(extention == 'php'){
			cmd = Config.getInstance().server.phpexe + ' ' + cmd
		}
		def os = OsResolver.getOs()
		if(OS.WINDOWS == os){
			cmd = 'cmd /c ' + cmd
		}
		return cmd
	}
	
	private List createEnv(Request req, File local, String extention) {
		def cgiEnv = [:]
		if(extention == 'php'){
			cgiEnv = new HashMap(System.getenv())
		}
		cgiEnv.putAll(generateVarsForRequest(req, local))
		cgiEnv = cgiEnv.collect{k,v -> "${k}=${v}"}
		return cgiEnv
	}

	byte[] handleIoOfCgiScript(Process proc, Request req, Map header) {
		def body = readHeaderAndBodyFromCgiStdOut(proc, header)
		return body
	}

	private forwardReqBodyToCgiViaStdIo(Process proc, Request req) {
		if(req.isBodyPresent()){
			def os = new BufferedOutputStream(proc.getOutputStream())
			os << req.getBody()
			os.close()
		}
	}
	
	private byte[] readHeaderAndBodyFromCgiStdOut(Process proc, Map header) {
		StringBuilder b = new StringBuilder()
		def isStillHeader = true
		proc.getInputStream().eachLine { line ->
			isStillHeader = handleLine(isStillHeader, line, header, b)
		}
		def body = b.toString().getBytes()
		return body
	}

	private boolean handleLine(boolean isStillHeader, String line, Map header, StringBuilder b) {
		if(isStillHeader){
			if(line.isEmpty()){
				isStillHeader = false
			} else {
				readToHeader(line, header)
			}
		}	else {
			readToBody(b, line)
		}
		return isStillHeader
	}

	private readToHeader(String line, Map header) {
		def kv = RequestParser.parseHeaderLine(line)
		header.put(kv.getFirst(), kv.getSecond())
	}

	def readToBody(StringBuilder b, String line) {
		b.append(line + Constants.CRLF)
	}

	def generateVarsForRequest(Request req, File local){
		def cgiEnv = [:]
		def header = req.getHeader()
		// handle content length and type
		extractAndPut(cgiEnv, header, Constants.CONTENT_LENGTH, Constants.CONTENT_LENGTH_CGI)
		extractAndPut(cgiEnv, header, Constants.CONTENT_TYPE, Constants.CONTENT_TYPE_CGI)
		// allow cgi without redirects
		cgiEnv.put(Constants.GATEWAY_INTERFACE_CGI, Constants.CGI_VERSION)
		// handle other header vars HTTP*
		handleOtherHeaderVars(header, cgiEnv)
//
//		// path info and query string		
		def cgiResource = URI.create(req.getResource())
		handlePathInfo(cgiResource, local, cgiEnv)
		handleQueryString(cgiResource, cgiEnv)
//			// REMOTE?
//		
		cgiEnv.put(Constants.REQUEST_METHOD_CGI, req.getMethod().toString())
		cgiEnv.put(Constants.SCRIPT_NAME_CGI, local.getPath())
		// host port
		handleHostPortVars(header, cgiEnv)
		// http version / server name
		cgiEnv.put(Constants.SERVER_PROTOCOL_CGI, Constants.HTTP_1_1)
		cgiEnv.put(Constants.SERVER_SOFTWARE_CGI, 'GroovyWebServer')
		cgiEnv
	}

	def extractAndPut(resultMap, valueMap, valueK, resultK){
		def v = valueMap.get(valueK)
		if(v){
			resultMap.put(resultK, v.toString())
		}
	}
	
	private handleOtherHeaderVars(Map header, Map cgiEnv) {
		header.findAll {k,v -> k != Constants.CONTENT_LENGTH  && k != Constants.HOST && k != Constants.CONTENT_TYPE}.each {k,v ->
			def keyUpperCaseUnderscore = k.replaceAll('-', '_').toUpperCase()
			cgiEnv.put(Constants.HTTP_PREFIX_CGI + keyUpperCaseUnderscore, v.toString())
		}
	}

	def handlePathInfo(URI cgiResource, File local, Map cgiEnv) {
		def path = cgiResource.getPath()
		def rootSize = root.getAbsolutePath().size() 
		def localSize = local.getAbsolutePath().size()
		def relativePathSize = path.size()
		def pathInfo = path.substring(localSize - rootSize, relativePathSize)
		if(pathInfo){
			cgiEnv.put(Constants.PATH_INFO_CGI, pathInfo)
			cgiEnv.put(Constants.PATH_TRANSLATED_CGI, root.getAbsolutePath() + pathInfo)
		}
	}

	private handleQueryString(URI cgiResource, Map cgiEnv) {
		def qs = cgiResource.getQuery()
		if(qs){
			cgiEnv.put(Constants.QUERY_STRING_CGI, qs)
		}
	}

	private handleHostPortVars(Map header, Map cgiEnv) {
		def hostPort = header.get(Constants.HOST).split(':')
		switch(hostPort.size()){
			case 2 :
				cgiEnv.put(Constants.SERVER_PORT_CGI, hostPort[1])
			case 1 :
				cgiEnv.put(Constants.SERVER_NAME_CGI, hostPort[0])
				break;
		}
	}
		
}
