package de.uni.leipzig.exception

class RequestParseException extends Exception {
	
	RequestParseException(Throwable ul, String message){
		super(message, ul)
	}
	
	RequestParseException(String message){
		super(message)
	}
	
}

class SyntaxRequestParseException extends RequestParseException {
	SyntaxRequestParseException(Throwable ul, String message){
		super(ul, message)
	}
	
	SyntaxRequestParseException(String message){
		super(message)
	}
}
class SemanticRequestParseException extends RequestParseException {
	SemanticRequestParseException(Throwable ul, String message){
		super(ul, message)
	}
	
	SemanticRequestParseException(String message){
		super(message)
	}
}
