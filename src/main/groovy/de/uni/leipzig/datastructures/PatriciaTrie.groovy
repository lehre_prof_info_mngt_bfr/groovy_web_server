package de.uni.leipzig.datastructures

import groovy.transform.EqualsAndHashCode
import groovy.transform.InheritConstructors
import groovy.transform.ToString

import java.util.AbstractMap
import java.util.Optional

@ToString
abstract class StringDifference {
	
	static def difference(s1, s2){
		if(s1.size() > s2.size()){
			checkForPartiallyEqual(s2, s1, {new SmallerCurrentNode(it.size(), s1, s2)})
		} else if(s1.size() < s2.size()){
			checkForPartiallyEqual(s1, s2, {new GreaterCurrentNode(it.size(), s2, s1)})
		}  else {
			checkForPartiallyEqual(s1, s2, {new Equal(it.size(), s1)})
		}
	}

	private static checkForPartiallyEqual(s1, s2, other) {
		def returnable
		def index = 0
		for(def letter : s1){
			def s2Letter = s2[index]
			if(s2Letter != letter){
				if(index == 0){
					returnable = new NotEqual()
				} else {
					returnable =  new PartlyEqual(index, s1, s2)
				}
				break;
			}
			index++
		}
		if(returnable == null){
			returnable = other(s1)
		}
		return returnable
	}
	
	int countOfEqualLetters
	String equalPart
	
	StringDifference(){}
	
	StringDifference(int countOfEqualLetters, String gtString){
		this.countOfEqualLetters = countOfEqualLetters
		equalPart = gtString.substring(0,countOfEqualLetters)
	}

	@Override
	public boolean equals(Object obj) {
		if(this.is(obj)){
			true
		} else if(obj == null){
			false
		} else if(obj.getClass().isAssignableFrom(this.getClass())){
			countOfEqualLetters == obj.countOfEqualLetters && this.equalPart == obj.equalPart
		} else {
			false 
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + countOfEqualLetters;
		return result;
	}
		
}

class NotEqual extends StringDifference {
	NotEqual(){
		super(0, '')
	}
}
@ToString @InheritConstructors class Equal extends StringDifference {}

@ToString @InheritConstructors class GreaterCurrentNode extends StringDifference {
	String notEqualPart
	
	GreaterCurrentNode(int countOfEqualLetters, String s2, String s1){
		super(countOfEqualLetters, s2)
		notEqualPart = s2.substring(countOfEqualLetters, s2.size())
	}
	
}

@ToString @InheritConstructors class SmallerCurrentNode extends StringDifference {
	String notEqualPart
	
	SmallerCurrentNode(int countOfEqualLetters, String s1, String s2){
		super(countOfEqualLetters, s1)
		notEqualPart = s1.substring(countOfEqualLetters, s1.size())
	}
}

@ToString @InheritConstructors class PartlyEqual extends StringDifference {
	
	String notEqualPart
	String notEqualPartKey
	
	PartlyEqual(int countOfEqualLetters, String s1, String s2){
		super(countOfEqualLetters, s1)
		notEqualPartKey = s1.substring(countOfEqualLetters, s1.size())
		notEqualPart = s2.substring(countOfEqualLetters, s2.size())
	}
}

trait PatriciaNode {
	abstract def add(String addable, Object value)
	abstract def remove(String removable)
	abstract def search(String findable)
	abstract def iterate()
	
	static copy(PatriciaNode other){
		if(other instanceof Leaf){
			return Leaf.INSTANCE
		} else if(other instanceof EmptyPatriciaNode){
			return new EmptyPatriciaNode(other)
		} else {
			return new CommonPatriciaNode(other)
		}
	}
	
}

enum Leaf implements PatriciaNode {
	
	INSTANCE

	String key = ''
	
	def add(String addable, Object value){
		new CommonPatriciaNode(key:addable, value:value)
	}
	
	def search(String findable){Optional.empty()}

	def remove(String removable){
		INSTANCE
	}
	
	def iterate(){
		[] as Set
	}
	
	@Override
	public String toString() {
		'<E>'
	}
	
}

@EqualsAndHashCode
class CommonPatriciaNode implements PatriciaNode {
	
	static def balance(equalPartKey, value, first, second){
		first = copy(first)
		second = copy(second)
		def returnable = new CommonPatriciaNode(key : equalPartKey, value : value)
		switch(first.getClass()){
			case EmptyPatriciaNode:
				switch(second.getClass()){
					case EmptyPatriciaNode:
						balanceTwoEmptyNodes(returnable, first, second)
						break
					case Leaf:
						balanceEmptyAndNormalNode(equalPartKey, first, returnable)
						break 
					case CommonPatriciaNode:
						insertAccordingToPatriciaTreeInvariant(returnable, first, second, equalPartKey)
						break
				}
				break
			case CommonPatriciaNode:
				switch(second.getClass()){
					case EmptyPatriciaNode:
						insertAccordingToPatriciaTreeInvariant(returnable, second, first, equalPartKey)
						break
					case Leaf:
						insertAccordingToPatriciaTreeInvariant(returnable, second, first, equalPartKey)
						break
					case CommonPatriciaNode:
						balanceTwoNormalNodes(returnable, second, first, equalPartKey)
						break
				}
				break
			case Leaf: 
				switch(second.getClass()){
					case Leaf:
						break 
					case EmptyPatriciaNode:
						balanceEmptyAndNormalNode(equalPartKey, second, returnable)
						break
					case CommonPatriciaNode:
						insertAccordingToPatriciaTreeInvariant(returnable, first, second, equalPartKey)
						break
				}
				break
				
		}
		returnable
	}

	private static balanceTwoEmptyNodes(CommonPatriciaNode returnable, first, second) {
		def sorted = [returnable, first.left, first.right, second.left, second.right].toSorted {a,b -> a.key <=> b.key}
		def indexOfRoot = sorted.indexOf(returnable)
		switch(indexOfRoot){
			case 0:
				returnable.right = EmptyPatriciaNode.linkedList(sorted[1..4])
				break
			case 1:
				returnable.left = sorted[0]
				returnable.right = EmptyPatriciaNode.linkedList(sorted[2..4])
				break
			case 2:
				returnable.left = new EmptyPatriciaNode(left : sorted[0], right : sorted[1])
				returnable.right = new EmptyPatriciaNode(left : sorted[3], right : sorted[4])
				break
			case 3:
				returnable.left = EmptyPatriciaNode.linkedList(sorted[0..2])
				returnable.right = sorted[4]
				break
			case 4:
				returnable.left = EmptyPatriciaNode.linkedList(sorted[0..3])
				break
		}
	}

	private static balanceTwoNormalNodes(CommonPatriciaNode returnable, second, first, equalPartKey) {
		if(first.key.compareTo(equalPartKey) > 0 && second.key.compareTo(equalPartKey) > 0){
			if(first.key.compareTo(second.key) > 0){
				returnable.right = new EmptyPatriciaNode(left : second, right : first)
			} else {
				returnable.right = new EmptyPatriciaNode(left : first, right : second)
			}
		} else if(first.key.compareTo(equalPartKey) < 0 && second.key.compareTo(equalPartKey) < 0) {
			if(first.key.compareTo(second.key) > 0){
				returnable.left = new EmptyPatriciaNode(left : second, right : first)
			} else {
				returnable.left = new EmptyPatriciaNode(left : first, right : second)
			}
		} else {
			insertAccordingToPatriciaTreeInvariant(returnable, first, second, equalPartKey)
		}
	}

	private static balanceEmptyAndNormalNode(equalPartKey, second, CommonPatriciaNode returnable) {
		def gtLeft = equalPartKey.compareTo(second.left.key) > 0
		def gtRight = equalPartKey.compareTo(second.right.key) > 0
		if(gtLeft && gtRight){
			returnable.left = second
		} else if(gtLeft && !gtRight){
			returnable.left = second.left
			returnable.right = second.right
		} else {
			returnable.right = second
		}
	}

	private static insertAccordingToPatriciaTreeInvariant(CommonPatriciaNode returnable, first, second, equalPartKey) {
		if(second.key.compareTo(equalPartKey) > 0){
			returnable.left = first
			returnable.right = second
		} else {
			returnable.left = second
			returnable.right = first
		}
	}
	
	PatriciaNode left = Leaf.INSTANCE, right = Leaf.INSTANCE
	Object value
	String key

	CommonPatriciaNode(){}
	
	public CommonPatriciaNode(CommonPatriciaNode other) {
		this.left = other.left;
		this.right = other.right;
		this.value = other.value;
		this.key = other.key;
	}

	def add(String addable, Object value) {
		def returnable = this
		def difference = StringDifference.difference(key, addable)
		switch(difference.getClass()){
			case Equal: 
				returnable = new CommonPatriciaNode(key : addable, value : value, left : copy(left), right: copy(right))
				break
			case NotEqual: 
				def oldKey = this.key
				def oldValue = this.value
				if(key.compareTo(addable) > 0){
					returnable = new EmptyPatriciaNode(left : new CommonPatriciaNode(key : addable, value : value), right : copy(this))
				} else {
					returnable = new EmptyPatriciaNode(left : copy(this), right : new CommonPatriciaNode(key : addable, value : value))
				}
				break
			case SmallerCurrentNode:
				def equalPart = addable
				def notEqualPart = difference.notEqualPart
				def balancedSubtree = balance(notEqualPart, this.value, left, right)
				if((equalPart <=> notEqualPart) > 0){
					returnable = new CommonPatriciaNode(key : equalPart, value : value, left : balancedSubtree)	
				} else {
					returnable = new CommonPatriciaNode(key : equalPart, value : value, right : balancedSubtree)
				}
				break
			case GreaterCurrentNode:
				def notEqualPart = difference.notEqualPart
				if(key.compareTo(notEqualPart) > 0){
					returnable = new CommonPatriciaNode(key: key, value : this.value, left : left.add(notEqualPart, value), right: copy(right))
				} else {
					returnable = new CommonPatriciaNode(key: key, value : this.value, left : copy(left), right: right.add(notEqualPart, value))
				}
				break
			case PartlyEqual:
				def newLeft = Leaf.INSTANCE	
				def newRight = Leaf.INSTANCE
				def equalPart = difference.equalPart
				def notEqualPartKey = difference.notEqualPartKey
				def notEqualPartAddable = difference.notEqualPart
				returnable = new CommonPatriciaNode(key : equalPart, value : null)
				def balancedSubtree = balance(notEqualPartKey, this.value, left, right)
				if((equalPart <=> notEqualPartKey) > 0){
					newLeft = balancedSubtree
				} else {
					newRight = balancedSubtree 
				}
				if((equalPart <=> notEqualPartAddable) > 0){
					newLeft = newLeft.add(notEqualPartAddable, value)
				} else {
					newRight = newRight.add(notEqualPartAddable, value)
				}
				returnable =  new CommonPatriciaNode(key : equalPart, value : null, left : newLeft, right : newRight )
				break
		}
		returnable
	}
	
	def search(String findable){
		def difference = StringDifference.difference(key, findable)
		switch(difference.getClass()){
			case Equal:
				if(value == null){
					return Optional.empty()
				} else {
					return Optional.of(value)
				}
			case NotEqual: 
			case SmallerCurrentNode:
			case PartlyEqual:
				return Optional.empty()
			case GreaterCurrentNode:
				if((difference.notEqualPart <=> key ) > 0){
					return right.search(difference.notEqualPart)
				} else {
					return left.search(difference.notEqualPart)
				}
		}	
	}
	
	def remove(String removable){
		def difference = StringDifference.difference(key, removable)
		def returnable 
		switch(difference){
			case Equal:
				if(left == Leaf.INSTANCE && right == Leaf.INSTANCE){
					returnable = Leaf.INSTANCE
				} else {
					returnable = new CommonPatriciaNode(key : this.key, value : null, left : copy(left), right : copy(right))
				}
				break
			case GreaterCurrentNode:
				if((difference.notEqualPart <=> key) > 0){
					returnable = new CommonPatriciaNode(key : this.key, value : this.value, left : copy(left), right : right.remove(difference.notEqualPart))
				} else {
					returnable = new CommonPatriciaNode(key : this.key, value : this.value, left: left.remove(difference.notEqualPart) , right : copy(right))
				}
				if(returnable.left == Leaf.INSTANCE && returnable.right == Leaf.INSTANCE && this.value == null){
					returnable = Leaf.INSTANCE
				}
				break
			default: 
				returnable = copy(this)
				break
		}
		return returnable
	}
	
	def iterate(){
		def kv = [key, value] as Tuple2 
		def addPrefix = {tuple ->
			[key + tuple.getFirst(), tuple.getSecond()] as Tuple2
		}
		def left = left.iterate().collect(addPrefix) 
		def right = right.iterate().collect(addPrefix)
		if(value == null){
			left + right
		} else {
			left + [ kv ] + right
		}
	}
	
	def String toString() {
		"""'${key}'(${left},${right})""".stripMargin()
	}

}


@InheritConstructors
class EmptyPatriciaNode extends CommonPatriciaNode {
	
	static EmptyPatriciaNode linkedList(List patriciaNodes){
		def returnable = Leaf.INSTANCE
		patriciaNodes.reverse().each { node ->
			returnable = new EmptyPatriciaNode(left : node, right : returnable)
		}
		return returnable
	}
	
	EmptyPatriciaNode(){
		super(key : '', value : null)
	}

	@Override
	public Object add(String addable, Object value) {
		def comp = left.key[0].compareTo(addable[0])
		def returnable = this
		if(comp > 0){
			returnable = new EmptyPatriciaNode(left : new CommonPatriciaNode(key : addable, value : value), right : copy(this))
		} else if(comp == 0) {
			returnable = new EmptyPatriciaNode(left : left.add(addable, value), right : copy(right))
		} else {
			returnable = new EmptyPatriciaNode(left : copy(left), right : right.add(addable, value))
		}
		return returnable
	}
	
	def search(String findable){
		def leftResult = left.search(findable)
		if(leftResult.isPresent()){
			leftResult
		} else {
			right.search(findable)
		}
	}
	
	def remove(String removable){
		def newLeft = left.remove(removable)
		def newRight = right.remove(removable)
		if(newLeft.getClass() == Leaf){
			newRight
		} else if(newRight.getClass() == Leaf){
			newLeft
		} else {
			new EmptyPatriciaNode(left : newLeft, right : newRight)
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			false
		if(obj instanceof EmptyPatriciaNode && left.equals(obj.left) && right.equals(obj.right))
			true
		return super.equals(obj);
	}
}

class PatriciaTrie extends AbstractMap<String, Object> {

	PatriciaNode node = Leaf.INSTANCE
	
	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		node.iterate().collect {pair -> 
			new MapEntry(pair.getFirst(), pair.getSecond())
		}
	}

	/* 
	 * Trie add
	 * (non-Javadoc)
	 * @see java.util.AbstractMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Object put(String key, Object value) {
		node = node.add(key, value)
	}

	/* 
	 * Trie search
	 * (non-Javadoc)
	 * @see java.util.AbstractMap#get(java.lang.Object)
	 */
	@Override
	public Object get(Object key) {
		Optional ret = node.search(key)
		if(ret.isPresent())
			ret.get()
		else 
			null
	}

	/* 
	 * Trie remove
	 * (non-Javadoc)
	 * @see java.util.AbstractMap#remove(java.lang.Object)
	 */
	@Override
	public Object remove(Object key) {
		node.remove(key)
	}
}
