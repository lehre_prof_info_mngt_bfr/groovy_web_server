package de.uni.leipzig.com


import static de.uni.leipzig.constant.Constants.CONTENT_LENGTH
import static de.uni.leipzig.constant.Constants.CRLF

import java.util.HashMap;
import java.util.Map;

import groovy.transform.PackageScope
import groovy.transform.builder.Builder
import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpMethods

@Builder
class Request implements HttpMessage {

	static Request fromInputSteam(InputStream is){
		return new RequestParser(is).parse()
	}
	
	@PackageScope
	Request(){}
	
	// Request line fields
	@PackageScope
	String protocolVersion
	
	@PackageScope
	String resource 
	
	@PackageScope
	HttpMethods method
	
	@PackageScope
	HashMap<String, ?> header
	
	@PackageScope
	byte[] body
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(body);
		result = prime * result + ((header == null) ? 0 : header.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((protocolVersion == null) ? 0 : protocolVersion.hashCode());
		result = prime * result + ((resource == null) ? 0 : resource.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this.is(obj))
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (!Arrays.equals(body, other.body))
			return false;
		if (header == null) {
			if (other.header != null)
				return false;
		} else if (!header.equals(other.header))
			return false;
		if (method != other.method)
			return false;
		if (protocolVersion == null) { 
			if (other.protocolVersion != null)
				return false;
		} else if (!protocolVersion.equals(other.protocolVersion))
			return false;
		if (resource == null) {
			if (other.resource != null)
				return false;
		} else if (!resource.equals(other.resource))
			return false;
		return true;
	}
	
	String getProtocolVersion() {
		return protocolVersion;
	}

	String getResource() {
		return resource;
	}

	HttpMethods getMethod() {
		return method;
	}
	
	Map<String, ?> getHeader(){
		header
	}

	byte[] getBody(){
		body
	}

	public String getFirstLine() {
		return createRequestLine();
	}
	
	String createRequestLine() {
		"${method.toString()} ${resource} ${protocolVersion}"
	}
	
}
