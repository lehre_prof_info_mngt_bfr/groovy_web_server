package de.uni.leipzig.com

import static de.uni.leipzig.constant.Constants.CONTENT_LENGTH
import groovy.transform.PackageScope

import com.google.common.io.ByteArrayDataOutput
import com.google.common.io.ByteStreams

import de.uni.leipzig.constant.Constants
import de.uni.leipzig.constant.HttpMethods
import de.uni.leipzig.exception.RequestParseException
import de.uni.leipzig.exception.SemanticRequestParseException
import de.uni.leipzig.exception.SyntaxRequestParseException

class RequestParser {
	
	@PackageScope
	static Integer BUFFER_SIZE = 1024 * 1024
	
	/**
	 * This function gets the next line from an arbitrary input stream.
	 * Thereby the next line is denoted by CRLF.
	 * Needed because the Java Reader classes are not handling the empty CRLF line of the HTTP protocol properly.
	 *
	 * @param is
	 * @return The next line in the stream
	 */
	@PackageScope
	def static String getNextLine(InputStream is){
		int written = 0, current;
		boolean foundCr = false;
		ByteArrayDataOutput out = ByteStreams.newDataOutput(BUFFER_SIZE);
		while((current = is.read()) != -1 && (!foundCr || current !=  10)){
			foundCr = current == 13;
			if(!foundCr){
				out.write(current)
			}
		}
		new String(out.toByteArray(), Constants.UTF_8)
	}
	
	InputStream is
	
	RequestParser(InputStream is){
		this.is = new BufferedInputStream(is)
	}
	
	RequestParser(BufferedInputStream is){
		this.is = is
	}
	
	Request parse() throws RequestParseException {
		try {
			def request = parseSyntactically()
			// semantic protocol tests
			request
		} catch(RequestParseException e){
			throw e
		} catch(Exception e){
			throw new RequestParseException(e, e.getMessage())
		}
	}
	
	private def parseSyntactically(){
		def returnable = Request.builder()
		parseRequestLine(returnable)
		def headers = parseHeader()
		returnable.header(headers)
		String contentLength = headers.get(CONTENT_LENGTH)
		returnable.body(parseBody(contentLength))
		return returnable.build()
	}

	private def parseRequestLine(returnable) {
		def firstLineParts = getNextLine(is).split(' ')
		if(firstLineParts.length == 3){
			try {
				returnable.method(HttpMethods.valueOf(firstLineParts[0]))
			} catch(IllegalArgumentException e){
				throw new SyntaxRequestParseException(e, 'The HTTP method was not found!')
			}
			returnable.resource(firstLineParts[1])
			returnable.protocolVersion(firstLineParts[2])
		} else {
			throw new SyntaxRequestParseException('status line broken')
		}
	}

	private Map parseHeader() {
		def headers = [:]
		String currentLine;
		while(!(currentLine = getNextLine(is)).isEmpty()){
			def kv = parseHeaderLine(currentLine)
			def key =  kv.getFirst()
			def value = kv.getSecond()
			switch(key){
				case CONTENT_LENGTH:
					if(headers.containsKey(CONTENT_LENGTH)){
						throw new SemanticRequestParseException('multiple content lengths')
					} else {
						value = Integer.parseInt(value)
					}
				break;
			}
			headers.put(key, value)
		}
		if(!headers.containsKey(Constants.HOST)){
			throw new SemanticRequestParseException('Request need to have host header!')
		}
		return headers
	}
	
	public static Tuple2 parseHeaderLine(String currentLine){
		def result = []
		def splitedLine = currentLine.split(':')
		if(splitedLine.size() < 2){
			throw new SyntaxRequestParseException("header format not readable ${splitedLine}")
		}
		def key = splitedLine[0]
		def indexsize = splitedLine.size() - 1
		def value = splitedLine[(1..indexsize)].join(':').trim()
		[key, value] as Tuple2
	}

	private def parseBody(contentLength) {
		def thereIsAContentLengthBody = contentLength != null
		byte[] body;
		if(thereIsAContentLengthBody){
			int cLength = Integer.parseInt(contentLength)
			body = new byte[cLength]
			int reallyRead = is.read(body, 0, cLength)
			if(reallyRead != cLength){
				throw new SyntaxRequestParseException("Content-Size != content read: ${cLength} != ${reallyRead}")
			}
		} else {
			body = [] as byte[]
		}
		body
	}
	
}