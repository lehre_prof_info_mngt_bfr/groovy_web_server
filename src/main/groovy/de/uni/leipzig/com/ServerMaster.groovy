package de.uni.leipzig.com;

import groovyx.gpars.actor.DefaultActor
import de.uni.leipzig.http.Config
import de.uni.leipzig.http.RequestProcessor

class ServerActor extends DefaultActor {
	
	def serversocket
	def rp
	
	@Override
	protected void act() {
		loop {
			def socket = serversocket.accept()
			rp.http(socket)
		}
	}
	
	public void afterStart(){
		serversocket = new ServerSocket(Config.getInstance().server.port)
		rp = new RequestProcessor()
	}
}

class ServerMaster {

	def actor = new ServerActor()
	
	def start(){
		actor.start()
	}
	
	def stop(){
		actor.stop()
	}
	
	/**
	 * The main method to run the webserver.
	 * There are to configuration arguments.
	 * args[0] - Port listening to
	 * args[1] - Path to server root 
	 * 
	 * @author Dan Häberlein
	 *
	 */
	static def main(args){
		def config = Config.getInstance()
		switch(args.size()){
			case 2:
				config.server.root = args[1] 
			case 1:
				def serverPort = Integer.parseInt(args[0])
				config.server.port = serverPort
		}
		println("start the groovy web server...")
		println("Config:\n${config.toString()}")
		new ServerMaster().start().join()
	}
}
