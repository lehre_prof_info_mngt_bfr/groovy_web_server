package de.uni.leipzig.com

import java.util.HashMap;
import java.util.Map;

import groovy.transform.PackageScope
import groovy.transform.builder.Builder;
import static de.uni.leipzig.constant.Constants.CRLF

trait HttpMessage {
	abstract String getFirstLine();
	abstract Map<String, ?> getHeader();
	abstract byte[] getBody();
	
	String toString(){
		generateHeaderPart() + new String(getBody())
	}
	
	private def generateHeaderPart(){
		getFirstLine() + CRLF +
		generateHeaderStringRep() +
		CRLF
	}
	
	private String generateHeaderStringRep(){
		def header = getHeader()
		if(header.isEmpty()){
			''
		} else {
			header.collect{k, v -> "${k}: ${v.toString()}"}.join(CRLF) + CRLF
		}
	}
	
	def to(OutputStream os){
		os << generateHeaderPart()
		os << body
		os.flush()
	}
	
	def isBodyPresent(){
		getBody().length != 0
	}
}
