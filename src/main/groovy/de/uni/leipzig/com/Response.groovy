package de.uni.leipzig.com

import groovy.transform.PackageScope
import groovy.transform.builder.Builder

import java.util.Arrays;

import de.uni.leipzig.constant.HttpStatus

@Builder
class Response implements HttpMessage {

	@PackageScope
	HttpStatus statusCode
	
	@PackageScope
	String protocolVersion
	  
	@PackageScope
	Map<String, ?> header;
	
	@PackageScope
	byte[] body;
	
	public String getFirstLine() {
		return "${protocolVersion} ${statusCode}";
	}

	public Map<String, ?> getHeader() {
		return header;
	}

	public byte[] getBody() {
		return body;
	}
	
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	
	public String getProtocolVersion() {
		return protocolVersion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(body);
		result = prime * result + ((header == null) ? 0 : header.hashCode());
		result = prime * result + ((protocolVersion == null) ? 0 : protocolVersion.hashCode());
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.is(obj))
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Response other = (Response) obj;
		if (!Arrays.equals(body, other.body))
			return false;
		if (header == null) {
			if (other.header != null)
				return false;
		} else if (!header.equals(other.header))
			return false;
		if (protocolVersion == null) {
			if (other.protocolVersion != null)
				return false;
		} else if (!protocolVersion.equals(other.protocolVersion))
			return false;
		if (statusCode != other.statusCode)
			return false;
		return true;
	}
	
}
