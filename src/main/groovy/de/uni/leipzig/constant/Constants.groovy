package de.uni.leipzig.constant;

class Constants {


	static final String CRLF = "\r\n";
	static final String UTF_8 = "UTF-8";
	
	static final String CONTENT_LENGTH = "Content-Length";
	static final String CONTENT_TYPE = 'Content-Type'
	static final String SERVER_NAME = 'Server'
	static final String HOST = 'Host'
	
	static final String HTTP_1_1 = 'HTTP/1.1'
	static final String DATE = 'Date'
	
	static final String ROOT_DEFAULT = 'htdocs'
	static final int TIMEOUT = 1000 * 60
	
	static final String ERROR_MESSAGE_TIMEOUT = 'Took to long to request!'
	
	static final String AUTH_TYPE_CGI = 'AUTH_TYPE'
	static final String CONTENT_LENGTH_CGI = 'CONTENT_LENGTH'
	static final String CONTENT_TYPE_CGI = 'CONTENT_TYPE'
	static final String GATEWAY_INTERFACE_CGI = 'GATEWAY_INTERFACE'
	static final String HTTP_PREFIX_CGI = 'HTTP_'
	static final String PATH_INFO_CGI = 'PATH_INFO'
	static final String PATH_TRANSLATED_CGI = 'PATH_TRANSLATED'
	static final String QUERY_STRING_CGI = 'QUERY_STRING'
	static final String REMOTE_ADDR_CGI = 'REMOTE_ADDR'
	static final String REMOTE_HOST_CGI = 'REMOTE_HOST'
	static final String REMOTE_IDENT_CGI = 'REMOTE_IDENT'
	static final String REMOTE_USER_CGI = 'REMOTE_USER'
	static final String REQUEST_METHOD_CGI = 'REQUEST_METHOD'
	static final String SCRIPT_NAME_CGI = 'SCRIPT_NAME'
	static final String SERVER_NAME_CGI = 'SERVER_NAME'
	static final String SERVER_PORT_CGI = 'SERVER_PORT'
	static final String SERVER_PROTOCOL_CGI = 'SERVER_PROTOCOL'
	static final String SERVER_SOFTWARE_CGI = 'SERVER_SOFTWARE'

	static final String CGI_VERSION = 'CGI/1.1'
	
}
