package examples

class DummyPort80 {

	static class Request {
		static Request readHttp(InputStream httpFormatedStream){
			def returnable = new Request()
			Reader reader = httpFormatedStream.newReader()
			def firstLineParts = reader.readLine().split(' ')
			// insert checks
			returnable.method = firstLineParts[0]
			returnable.query = firstLineParts[1]
			returnable.protocol = firstLineParts[2]
			String currentLine;
			while(!(currentLine = reader.readLine()).isEmpty()){
				def splitedLine = currentLine.split(':')
				println(splitedLine)
				// insert checks
				
				returnable.headers.put(splitedLine[0], splitedLine[1].trim())
			}
		
			String contentLength = returnable.headers.get("Content-Length")
			def thereIsABody = contentLength != null
			if(thereIsABody){
				int cLength = Integer.parseInt(contentLength)
				byte[] body = new byte[cLength]
				httpFormatedStream.read(body, 0, cLength);
				returnable.body = body
			}
			return returnable
		}
		
		def method
		def query
		def protocol 
		
		def headers = [:]
		byte[] body
		
		String toString(){
			return headers
		} 
	}
	
	static main(args){
		println('Port 80 dummy starts!')
		def serverSocket = new ServerSocket(80)
		while(true){
			serverSocket.accept().withCloseable { Socket socket ->
					println("Where is the request comming from? --> ${socket.getRemoteSocketAddress()}")
					Request r = Request.readHttp(socket.getInputStream())
					println(r.toString())
					println(r.method)
					println(r.query)
					println(r.protocol)
					if(r.body != null){
						println(new String(r.body))
					}
					def os = socket.getOutputStream().newWriter()
					os.write("HTTP/1.1 200 OK\r\n\r\n")
					os.flush()
					socket.close()
				}
			}
	}
}
